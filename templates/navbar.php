<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">PHP E-COMMERCE</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../views/catalog.php">Catalog <span class="sr-only">(current)</span></a>
                </li>

                <?php
                session_start();

                if (isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 2) {

                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="../views/cart.php">Show Cart <span id="cartCount" class="badge bg-dark text-light">
                                <?php

                                    if (isset($_SESSION['cart'])) {
                                        echo array_sum($_SESSION['cart']);
                                    } else {
                                        echo 0;
                                    }
                                    ?></span></a>
                    </li>
                <?php

                } else if (isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="../views/add_item_form.php">Add Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../views/admin_orders.php">All Orders</a>
                    </li>
                <?php
                };

                if (isset($_SESSION['user'])) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Hello, <?php echo $_SESSION['user']['firstName'] ?> </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
                    </li>
                <?php
                } else {

                    ?>
                    <li class="nav-item">
                        <a class="nav-link" href="../views/login.php">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../views/register.php">Register</a>
                    </li>
                <?php
                }


                ?>










            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>

</body>

</html>