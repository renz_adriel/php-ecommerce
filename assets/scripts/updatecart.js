// Capture all inputs
// we'll attach event listener to each input
// third, if we remove focus from the input field, it should submit the form

let quantityInputs = document.querySelectorAll(".quantityInput");


quantityInputs.forEach(editInput => {
    editInput.addEventListener("change", () => {
        editInput.parentElement.submit();
    })
})