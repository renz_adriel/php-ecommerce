// add an event listener to the button
// capture the details
// create a new form data
// append the data
// use fetch to access our process_authenticate.php


loginUser.addEventListener("click", () => {
    let email = document.querySelector("#email").value;
    let password = document.querySelector("#password").value;

    let data = new FormData;

    // "email" --key name
    data.append("email", email);
    data.append("password", password);


    // eto parang ginagawa mo yung form sa html kaya connected na to sa php
    fetch("../../controllers/process_authenticate.php", {
        method: "POST",
        body: data
    }).then(response => {
        return response.text();
    }).then(data_from_fetch => {
        console.log(data_from_fetch);
        if (data_from_fetch == "login_failed") {
            document.querySelector("#email").nextElementSibling.innerHTML = "Please provide correct credentials";
        } else {
            // for js imbes na header location kay php
            window.location.replace("../../views/catalog.php");
        }
    })
})