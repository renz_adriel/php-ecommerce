<?php
require "../templates/template.php";


// VIEW THIS LATER


function get_content()
{
    ?>

    <h1 class="text-center py-5">CART</h1>
    <hr>

    <div class="table-responsive table">
        <table class="table table-striped">
            <thead>
                <tr class="text-center">
                    <th>Item</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>


                <?php
                    $total = 0;
                    // session_start();
                    require "../controllers/connection.php";

                    // 1. to check whether we have %_SESSION['cart']
                    // 2. if there is, get the session cart and display each in a <tr>
                    // a. check the dataytype of our session cart --associative array!
                    // b. do the loop forecah ($data as key => value) since it is an assoc array
                    // c. the goal is to get the id and the quantity
                    // d get the item details through item_query
                    // e. select * from items where id = the id we got from for each
                    // f. use mysqli_query to get individual item
                    // g. get the subtotal by multipying indiv_item['price'] and quantity we got from the for each
                    // h. get the total (in able for us to do so, we must initialize $total at the very top)
                    // i. diplay the details in <tr><td>

                    // var_dump($_SESSION['cart']);
                    //         die();

                    if (isset($_SESSION['cart'])) {
                        foreach ($_SESSION['cart'] as $item_id => $item_quantity) {
                            $item_query = "SELECT * FROM items WHERE id = $item_id";

                            $indiv_item = mysqli_fetch_assoc(mysqli_query($conn, $item_query));
                            $subtotal = $indiv_item['price'] * $item_quantity;

                            $total += $subtotal;


                            ?>

                        <tr class="text-center">
                            <td><?php echo $indiv_item['name'] ?></td>
                            <td><?php echo number_format($indiv_item['price'], 2) ?></td>
                            <td>
                                <form action="../controllers/process_update_cart.php" method="POST">
                                    <input type="hidden" name="id" value="<?php echo $item_id ?>">
                                    <input type="hidden" name="fromCartPage" value="true">
                                    <input class="form-control quantityInput" type="number" name="quantity" value="<?php echo $item_quantity ?>">
                                </form>

                            </td>
                            <td><?php echo number_format($subtotal, 2) ?></td>
                            <td> <a href="../controllers/process_remove_items.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-danger">Remove Item</a></td>
                        </tr>
                <?php
                        }
                    }


                    ?>
                <tr class="text-center">
                    <td></td>
                    <td></td>
                    <td>
                        <a href="../controllers/process_empty_cart.php" class="btn btn-danger">Empty Cart</a>
                    </td>
                    <td>Total: PHP <?php echo number_format($total, 2) ?></td>
                    <td>
                        <form action="../controllers/process_checkout.php" method="POST">
                            <button class="btn btn-info" type="submit">Pay via COD</button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <script type="text/javascript" src="../assets/scripts/updatecart.js"></script>
<?php
}
?>