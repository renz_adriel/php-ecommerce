<?php
require "../templates/template.php";

function get_content()
{
    require "../controllers/connection.php";

    ?>

    <h1 class="text-center py-5">CATALOG PAGE</h1>
    <div class="container">
        <div class="row">
            <!-- sidebar -->
            <div class="col-lg-2">
                <h3>Categories</h3>
                <ul class="list-group border">
                    <li class="list-group item">
                        <a href="catalog.php">All</a>
                    </li>
                    <?php
                        $categories_query = "SELECT * FROM categories";

                        $categories = mysqli_query($conn, $categories_query);

                        foreach ($categories as $categories) {
                            ?>
                        <li class="list-group item">
                            <a href="catalog.php?category_id=<?php echo $categories['id'] ?>"><?php echo  $categories['name'] ?></a>
                        </li>
                    <?php
                        }

                        ?>
                </ul>

                <!-- sorting -->
                <h3>Sort By</h3>
                <ul class="list-group border">
                    <li class="list-group item">
                        <a href="../controllers/process_sort.php?sort=asc">Price(Lowest to Highest)</a>
                    </li>
                    <li class="list-group item">
                        <a href="../controllers/process_sort.php?sort=desc">Price(Highest to Lowest)</a>
                    </li>
                </ul>
            </div>
            <!-- Item list -->
            <div class="col-lg-10">
                <div class="row">
                    <?php
                        // Steps for retrieving items
                        // 1. Create a query
                        // 2. Use mysqli_query to get the results
                        // 3. if array (use for each)
                        // 4. if object (use mysqli_fetch_assoc to convert the data to an associative array)
                        // 4.1 Use for each (result as $key => $value)

                        // review this later remove!
                        // session_start();



                        $items_query = "SELECT * FROM items";



                        // for filtering
                        if (isset($_GET['category_id'])) {
                            $cat_Id = $_GET['category_id'];
                            $items_query .= " WHERE category_id = $cat_Id";
                        };


                        // sort
                        if (isset($_SESSION['sort'])) {
                            $items_query .= $_SESSION['sort'];
                        }

                        $items = mysqli_query($conn, $items_query);

                        // var_dump($items);
                        // die();

                        foreach ($items as $indiv_item) {
                            ?>
                        <div class="col-lg-4 py-2">
                            <div class="card h-100">
                                <img class="card-img-top" src="<?php echo $indiv_item['image'] ?>" alt="image">
                                <div class="card-body">
                                    <h4 class="card-title"><?php echo $indiv_item['name'] ?></h4>
                                    <p class="card-text">Php <?php echo $indiv_item['price'] ?></p>
                                    <p class="card-text"><?php echo $indiv_item['description'] ?></p>
                                    <?php
                                            // process of displaying category
                                            // 1. get the categogory name where id = category id
                                            // 2. display the data
                                            $catId = $indiv_item['category_id'];
                                            $category_query = "SELECT * FROM categories where id = $catId";

                                            $category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
                                            // var_dump($category);
                                            // die();
                                            // for assoc array lagi ganto format ng for each may key value

                                            ?>
                                    <p class="card-text">Category: <?php echo $category['name'] ?></p>
                                </div>

                                <?php
                                        if (isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 1) {
                                            ?>
                                    <div class="card-footer">
                                        <a href="edit_item_form.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-secondary">Edit Item</a>
                                        <a href="../controllers/process_delete_item.php?id=<?php echo $indiv_item['id'] ?>" class="btn btn-danger">Delete Item</a>
                                    </div>
                                <?php
                                        } else {
                                            ?>
                                                <div class="card-footer">
                                                    <!-- <form action="../controllers/process_update_cart.php" method="POST">
                                                    <input type="number" class="form-control" value="1" name="quantity">
                                                    <input type="hidden" name="id" value="echo $indiv_item['id'] ?>">
                                                    <button class="btn btn-primary">Add To Cart</button>

                                                </form> -->

                                                    <input type="number" class="form-control" value="1">
                                                    <button type="button" class="btn btn-success addToCartBtn" data-id="<?php echo $indiv_item['id'] ?>">Add to Cart</button>
                                                </div>

                                    <?php
                                        }

                                    ?>



                            </div>
                        </div>

                    <?php
                        }

                        ?>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../assets/scripts/addToCart.js"></script>
<?php
}
?>