<?php
require "../templates/template.php";
function get_content()
{
    require "../controllers/connection.php";
    $order_query = "SELECT transaction_code, purchase_date, total, statuses.name
                   AS status, payments.name AS payment, firstName, lastName, email
                   FROM orders
                   JOIN (statuses,payments,users) ON (orders.status_id = statuses.id AND orders.payment_id = payments.id AND orders.user_id = users.id)";
    $orders = mysqli_query($conn, $order_query);
    ?>
    <h1 class="text-center py-5">ALL ORDERS</h1>
    <div class="table-responsive col-lg-10 offset-lg-1">
        <table class="table table-striped">
            <thead>
                <th>Transaction Code</th>
                <th>Customer Details</th>
                <th>Order Total</th>
                <th>Order Status</th>
                <th>Mode of Payment</th>
            </thead>
            <tbody>
                <?php
                    foreach ($orders as $indiv_order) {
                        ?>
                    <tr>
                        <td><?php echo $indiv_order['transaction_code'] ?>
                        </td>
                        <td>
                            Name: <?php echo $indiv_order['firstName'] . " " . $indiv_order['lastName'] ?>
                            <br>
                            Email: <?php echo $indiv_order['email'] ?>
                        </td>
                        <td>PHP <?php echo number_format($indiv_order['total'], 2) ?>
                        </td>
                        <td>
                            <span class="currentStatus">
                                <?php echo $indiv_order['status'] ?>
                            </span>
                            <select class="selectStatus" style="display:none" name="status_id">
                                <?php
                                        $status_query = "SELECT * from statuses";
                                        $statuses = mysqli_query($conn, $status_query);
                                        foreach ($statuses as $status) {
                                            ?>
                                    <option value="<?php echo $status['id'] ?>" <?php echo $indiv_order['status'] == $status['name'] ? "selected" : "" ?>><?php echo $status['name'] ?>
                                    </option>
                                <?php
                                        }
                                        ?>
                            </select>
                        </td>
                        <td><?php echo $indiv_order['payment'] ?>
                        </td>
                    </tr>
                <?php
                    }
                    ?>
            </tbody>
        </table>
    </div>
    <script src="../assets/scripts/editStatus.js" type="text/javascript"></script>
<?php
}
?>