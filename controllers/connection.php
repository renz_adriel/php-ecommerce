<?php 
    $db_host = "localhost";
    $db_username = "root";
    $db_password = "";
    $db_name = "b46ecommerce";

    // create a connection
    $conn = mysqli_connect($db_host, $db_username, $db_password, $db_name);

    // check if the connection is successful
    if(!$conn) {
        die("Connection failed: ".mysqli_error($conn));
    };
?>