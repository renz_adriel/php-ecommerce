<?php 

    session_start();
    $sort = $_GET['sort'];

    // we'll save the value of $sort in a session variable so we can use it in our view

    if(isset($sort)){
        if($sort == "asc"){
            $_SESSION['sort'] = " ORDER BY price ASC";
        } else {
            $_SESSION['sort'] = " ORDER BY price DESC";
        }
    };


    header("Location: " . $_SERVER[HTTP_REFERER]);


?>